# Script to draw random sample of firms to send off for transcription recording.


import pandas as pd
import numpy as np

df = pd.read_csv('data.csv')

df = df.loc[:,['FirmID','FirmName','IncentivePay_Reconciled','DiscretionaryPct_Reconciled']]

both_null = df[df.IncentivePay_Reconciled.isnull() & df.DiscretionaryPct_Reconciled.isnull()]

# Calling these zero.
neg_null = df[(df.IncentivePay_Reconciled < 0) & df.DiscretionaryPct_Reconciled.isnull()]
null_neg = df[df.IncentivePay_Reconciled.isnull() & (df.DiscretionaryPct_Reconciled < 0)]
neg_neg = df[(df.IncentivePay_Reconciled < 0) & (df.DiscretionaryPct_Reconciled < 0)]

picked_ids = [2258,3267,35,91,259,421]
bogus_ids = both_null.FirmID.tolist()

picked_ids.extend(bogus_ids)

ids = set(np.random.choice(df.FirmID.values,40))

ids.difference_update(set(picked_ids))
idx = df.FirmID.isin(ids)

for i,obs in df[idx].iterrows():
    print("{0:d}\t{1:s}".format(obs.FirmID,obs.FirmName))


