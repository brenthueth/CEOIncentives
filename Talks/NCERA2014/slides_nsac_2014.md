% Firm Ownership and CEO Incentives:  
  Evidence from Member-Owned Firms
% Brent Hueth and Philippe Marcoul
% October 5, 2014  
  NCERA 210, Minneapolis, MN

# CEO Incentives in Publicly Traded Companies

![Frydman and Jenter, 2009](frydman_jenter.png)

# Comparison with Private Companies

![Gao and Li, 2014](public_private.png)

# Coverage for our data 

![Interviews by sector, CEO and BC](coverage.png)

# CEO Incentives in Cooperative Firms

![Hueth and Marcoul, 2014](explicit_implicit.pdf)

# Drivers of Incentive Pay in Cooperative Firms

![Regression Results](results_explicit.png)

# Summary of key findings

- Co-ops use relatively "low-powered" incentives
- Co-ops commonly use implicit/subjective performance incentives
- Bigger co-ops use high-powered incentives (opposite finding, relative to public and private firms)
- Producer co-ops use higher-powered incentives than consumer cooperatives

# Understanding results

1. Agency problem
    - Task 1: "Bottom-line" firm performance
    - Task 2: Patron service flow (it's subjective)
    - Explicit contracting on bottom-line incentives
    - Implicit contracting on patron service flow
    - Two contracting frictions:
        - Multitasking problem for manager; task assignments conflict by a
           matter of degree that varies across firms
        - Difficulty of service task is not known at the time of contracting;
           manager learns after contract terms are set

. . .

2. Collective-choice problem:
    - Large number of members implies lower average benefit of service flow

# Central Hypotheses

1. Lower-powered performance incentives (on both tasks) when tasks interfere.
2. Higher-powered incentives (on both tasks) with higher monitoring precision.
2. High-powered "bottom-line" incentives with larger membership.


# Implications

- How do we measure "member value" and reward its creation appropriatley?
- Improve subjective evaluation process and design
- Do measurable criteria override non measurable, but more important, ones? What is the right balance?