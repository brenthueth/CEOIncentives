##### TODOS

- Read model and summarize results for empirical analysis.
- Generate new results with monitoring variables as explanatory
- Note that these can be viewed as endogenous and will be subject of future work
- Make sideways tables
- Have multiple regressions with a) only base controls; b) monitoring controls; c) demographics, d) sectoral controls -- each on implicit and incentive.
- Write description of significant differences with IOFs
- Run different regression for consumer and producer cooperatives
- Add conclusion

#### Comments

For CEO model, make problem function of n, and get ride of objective
function. Make it bargaining among as is but with CEO off to side
getting incentives…

Main prediction of model is that there’s there’s costly bargaining….

This is consistent with Hansmann hypothesis and nobody else has done
testing (including him!).

Are there competing hypotheses? See Schaeffer… can our model nest
Schaeffer? There’s lots of reasons in his model that things could go
other direction.

We also consider interaction between explicit and implicit… Philippe and
I need to look at this…  And for two other papers in implicit
incentives!! ExplicitImplicit, and Cocoa paper. And another one wine
contracts.

Do more general analysis effect of n and co-op type on dichotomous
choice explicit; and various levels of explicit; and correlation between
explicit and implicit.

How do you look at conditional distribution of dichotomous variable. I
guess you predict value taking away the conditioning variables (various
aggregations of sector effects).



##### Interpreting the effect of firm size (measured by number of members) on the value of noncontractible benefit:

- Horizontal heterogeneity. One individual gets what they want; with 2 they have to compromise; when there are 3 or more the compromise becomes bigger. Direction for management becomes less valuable to everyone. Can we build a model for this that is a reduced form for our \(\pi(n)\).?

##### Miscellaneous notes

- \(p_s\) signal is number of members, for example. This is not a perfect indicator of performance. 
- In Gibbons \(\mu\) is conractible and there is only one task.
- An imperfect signal is leads to friction and 2nd best outcome; observed signal vs. true performance
- Board commits to \(x^*\) is its exogenous.

#### Philipp's notes

- one clear issue is that we do not know when implicit incentives/explicit are
  more likely in the model. What parameters of the model favor the occurrence
  of a particular type of incentives unambiguously? More work on the
  theoretical model need to be accomplished. This will mean that we have to
  compute corner solutions and compare them to be in a position to
  (theoretically) predict when one type of incentives is more likely

- As of now, the model focuses too much on contractible tasks. It should be
  more focused on implicit incentives upfront. More work is needed to
  understand to what extend the fully contractible case differs to the case
  where service is not contractible

- We need to introduce CEO's idiosyncrasies (different cost of effort or
  different discount factor) in the theoretical model to account for the
  empirical results we have pertaining to CEO level of education.

- The model does not allow for a bonus rewarding both bottom line and service
  to members. This point needs to be acknowledged and addressed (possibly in a
  footnote). In particular we need to convey the idea that the introduction of
  this new modelling feature would not change our empirical predictions (so
  far, easier said than done!)

- Need to write a section on IOFs incentive schemes to contrast them with our
  empirical findings. This will provide us with a critical additional empirical
  results. A meta-analysis would probably be out of the scope of this current
  work.

- New proxies for "tensions within the membership" (i.e. for theta) need to be
  tested (this data exists). These proxies are described in the PDF manuscript
  document sent to you earlier today. For instance, using a measure of the
  amount invested in the cooperative may proxy well the importance that member
  attach to the bottom line in a producer organization but may also proxy well
  expectation with respect to service delivery in a consumer coop.

- The magnitude of polarization of incentives (i.e. the probability that we
  will observe explicit incentives or implicit incentives but not both) need to
  be regressed against the new proxies introduced in 6/

- All the regression we have (in particular those with implicit incentives)
  need to be re-run for producer cooperatives to see if results will be
  different.
