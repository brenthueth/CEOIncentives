% !TEX root = main.tex

% 1. State the question; don't cite anyone
% 2. Summarize the most relevant literature
% 3. The purpose of this paper is to... summarize what we do
% 4. The contribution of this work is to ... relative to prior literature.
% 5. Summarize results. Tell answer so reader can follow dev. of paper
% 6. Road map

Economists typically model the agency relationship between a firm's owners and
its manager as an informational problem. The board wishes to provide the
manager incentives to take costly unobserved actions that are value
maximizing, but the manager may choose to conceal or distort information that
is relevant to incentive design, and to manipulate performance measures
\emph{ex post}. However, the firm's owners generally agree on the set of
metrics that proxy for value. In this paper, we consider an agency problem
where the firm's owners each receive idiosyncratic transaction benefits from
the firm that do not show up perfectly in standard ``bottom-line'' measures of
firm financial performance. In this setting, managing a firm is partly a \emph
{collective-choice} problem where differing preferences must be reconciled for
the purpose of making unified firm-level decisions. In theory, collective
choices can be value maximizing, but they may not be if transaction costs
limit the use of lump-sum transfers across owners. We examine empirically the
importance of these costs by considering the outcome of one particular
transaction: setting performance-based pay for the CEO.

There is no consensus view on how to model the firm, despite many insightful
contributions \citep[e.g.,][]{coase1937,williamson1975,hart1995,spulber2009}.
\cite{hansmann1996} argues forcefully that collective-decision making costs
are responsible for the relative scarcity of member-owned firms (e.g.,
cooperatives, mutuals, and credit unions) in modern economies. In the absence
of a well-functioning market for firm ownership, it is impossible to separate
investors' consumption and investment behavior. This leads to inefficient
investment decisions, which compound any direct collective-decision making
costs that may exist as a result of rent-seeking and bargaining among members.
Despite compelling theory and some descriptive evidence that support the
existence of these costs, there is surprisingly little formal empirical
verification of their existence and magnitude. Further, to the extent that the
cooperative firm is seen as a partial remedy for market frictions, it is
impossible to evaluate the net impact of these costs without also considering
countervailing benefits that occur through other channels.\footnote{For example, a statutory constraint on access to investor financing may result in observed ``credit rationing'' and underinvestment at the level of the firm, but also in pricing behavior that privileges patrons over investors.} 

The purpose of this paper is to evaluate the effect of member ownership on
firm-level behavior. Although investment behavior and productivity measurement
are perhaps the most direct means of assessing behavior, there are several
challenges to performing such measurement in practice. Perhaps the most
obvious difficulty is access to firm-level data and observation of appropriate
comparison firms. In fact, it is quite difficult to find an ``apples-to-
apples'' comparison of a member-owned firm with an otherwise identical
investor-owned firm. Member-owned firms tend to be smaller than their
investor-owned counterparts, or to operate in unique sectors of the economy
where there are limited numbers of other kinds of firms that can be used for
comparison. We look instead at indirect evidence in the form of pay structure
for the CEO. There is a large literature on this topic for public and non-profit firms so we do not try to replicate any of that work. Instead, we
analyze the pay practices of a significant sample of member-owned firm across
all the major sectors where member-owned businesses are active in the U.S.
economy.

We motivate our empirical analysis with a simple agency model where the CEO is
payed to maximize the weighted sum of profit and the utility of the median
member. Weights depend on the relative difficulty of success with respect to
each measure, and we assume that the payoff to the median member decreases in
the number of members. This assumption is a reduced form for the notion that
satisfying the median member becomes more costly as the number of members
increases. We examine predictions of our model with the sample of firms
represented by our data, and we compare the results of our analysis with the
literature on pay structure for CEOs within investor-owned firms. The
contribution of our work is to provide an explicit model of pay structure in
the context of a member-owned firm, and to report formal evidence on
measurable differences between member-owned and investor-owned firms with
regard to how they choose to pay their CEOs. The evidence we report supports
the predictions of our model, and is consistent with the intuition that
member-owned firms, as they mature and grow large, become more like investor-owned firms. This behavior can be viewed as an efficient response to the
difficulty associated with satisfying an increasingly heterogeneous membership
population.

Summarizing briefly, our model makes two sets of related predictions, one with
regard to a comparison of like firms, but where there is no conflict between
value maximization and member preferences (or where the conflict is expected
to be small), and another with regard to the small-versus-large (measured by
the number of members) firms in our sample. In particular, our model suggests
that the existence of a conflict leads to relatively low-powered incentives
overall. When a firm has a relatively large number of members, the bottom-
line bonus increases, while the service-to-members bonus decreases.  The
intuition is that it becomes efficient to focus on bottom-line incentives
when the conflict among member preferences is large. Finally, a firm's board
with relatively imprecise information about service performance will also rely
more heavily on bottom-line incentives. In this setting, there value of
contracting on idiosyncratic member value is relatively low. We find strong
evidence in support of these predictions.

In what follows, we briefly review the literature on CEO pay structure that is
relevant to our analysis. We then develop a model of incentive design in a
setting where managers face explicit incentives based on a bottom-line
measure of firm performance, and implicit incentives based on the preferences
of the median member. We summarize predictions from our model, present our
data, and discuss estimation results. The final sections discuss results,
offer direction for future work, and provide concluding comments.

%summarized by the following set of key findings:
% Use specific numbers from our study and other existing studies.
% \begin{enumerate}
% 	\item Member-owned firms use far less highly-powered incentives than their investor-owned counterparts, use implicit incentives far more often, and monitor much more intensively.
% 	\item Relatively large member... Use
% \end{enumerate}


