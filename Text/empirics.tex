% !TEX root = main.tex

We examine predictions of the model presented in the previous section using
data collected from a sample of 485 cooperative firms. Our central hypotheses
is that cooperative firms must balance a desire for ``bottom line''
performance against the interests of members in service flows that may harm
the bottom line. All else equal, this hypothesis generates the following pair
of predictions:

\begin{enumerate}
	\item Greater conflict (higher $\theta$) and better monitoring precision (lower $\sigma_{\mu}^2$) lead to smaller performance-based incentives.
	\item Larger membership (higher $n$) leads to more highly powered
	``bottom-line'' incentives, and less focus on service flow.
\end{enumerate}

We test the predictions in three ways. First, we compare our data with that
from evidence collected and reported by others from the universe of
investor-owned firms. There is a vast literature on performance-based pay for
CEOs in these firms, but we are not aware of other similar data collected for
cooperative firms. We refer to comparison between cooperative and
noncooperative firms as ``across'' evidence. Second, we consider ``within''
evidence in our data as variaion across producer and consumer cooperatives,
which have a clear qualitative distinction that plausibly alters the nature of
the conflict between bottom-line performance and service flow.\footnote{Some
authors distinguish between producer and consumer cooperatives based on the
\emph{actions} of patrons, rather than their \emph{objective} as producers or
consumers. Under a distinction based on actions, a farmer cooperative that
sells farm inputs to its patrons might be defined as a consumer cooperative of
farmers who buy inputs, while a farmer cooperative that processes and markets
farmers’ outputs might defined as a producer cooperative of farmers who each
sell outputs. But whether farmers are buying or selling things, they remain
producers whose desire or objective plausibly is to earn income that they will
use to purchase consumption goods (this is not to say they don’t also get some
utility from their work as farmers). Similarly, a collection of retail hardware
stores that together purchase supplies for their stores are acting together as
producers, each who is income-seeking. We take the view that the distinction
based on objectives, rather than actions, is more salient for the purpose of
explaining how cooperatives how function. Farmer cooperatives that sell inputs
have a character that is much more like farmer cooperatives that process and
market farm supplies, than one that is like that of consumer-owned grocery
stores or credit unions.} We also consider within variation in monitoring intensity, firm sector, and CEO characteristics, and interpret results in light of our theory.

The data were gathered in 2011-2012 with phone interviews, typically lasting
between 40-60 minutes, of each firm's CEO. For each individual, we collected
information such as age, education level, tenure at the firm, and prior
experience with cooperative businesses. Similarly, we collected background
information about the business itself: age, reason for forming; current terms
of membership; general description of membership classes; economic sector; and
basic financial data. We also asked each CEO to send a hardcopy or electronic
version of their current bylaws, membership agreement, and most recent annual
report. Subsequent questions asked survey respondents to describe specific
governance and compensation (for the CEO and board members) practices at the
firm. For the purpose of this study we focus on the structure of
performance-based compensation for the CEO, and on questions regarding
monitoring activities of the board.

Table \ref{tab:data_summary} summarizes the data used in our analysis. We
selected firms into our study population through a series of stratified random
samples of the national census of cooperative firms conducted by the University
of Wisconsin Center for Cooperatives Research.\footnote{Please visit {\tt
http://reic.uwcc.wisc.edu} for details on this project.} Firms sampled come
from a wide range of economic sectors that for the purpose of analysis we
collapse into the 5 shown in table \ref{tab:data_summary}. Within the Finance
sector are credit unions, mutual insurers, and farm credit lenders. Utilities
include water, electric, and phone cooperatives. Withing the Social sector are
healthcare, arts and crafts, childcare, and media cooperatives. We do not have
any specific hypothesis with regard to variation in incentive and monitoring
activities across sectors. These controls are included in the analysis to
account for sector specific unobserved heterogeneity. The distinction between
Producer and Consumer cooperatives is important for us to the extent that it
captures variation in the degree of conflict among members about the objective
of the organization. Producer cooperatives are made up members who primarily
are interested in enhancing their own enterprise income. In contrast, members
in a consumer cooperative seek consumption utility directly. It is therefore
natural to expect much greater heterogeneity across members with in a consumer
cooperative, relative to a producer cooperative.

Our sample is stratified strongly toward cooperatives from the agricultural
sector, but several other sectors are well represented. Consumer cooperatives
tend on average to be larger than producer cooperatives. On average,
respondents reported that approximately \%17 of their annual pay is based on
measured performance. From the table, it appears that producer cooperatives
tend to rely more heavily on performance incentives than cooperatives. Of those
reporting performance-based pay, the Implicit column reports the average
spondents who indicated that average percentage of performance-based pay that
is ``implicit'' in the sense that it is determined by subjective criteria (no
by formula) and his not specified \emph{ex ante}. Figure \ref{fig:comp_hist}
present histograms of performance-baed pay and implicit incentives across the
firms in our sample. Interestingly, there are significant number of firms that
offer \emph{no} performance incentives, and some that base all pay on
performance.  Implicit incentives tend most often to be ``all or nothing'':
when a cooperative provides performance-based pay it is mostly likely to offer
no implicit incentives, or to offer them entirely based on subjective criteria.
With the exception of cooperatives in the agricultural sector, producer
cooperatives tend to hire more highly educated CEOs, and the average tenure for coorpeative CEOs is about 16 years.

\begin{figure}
\centering
\includegraphics[scale=1]{../Empirics/pbp_implicit.pdf}
\caption{Histograms of explicit and implicit performance-based pay.}
\label{fig:comp_hist}
\end{figure}

The Budget, Supervision, and Spending columns report average scores for codes
assigned by our interviewers to interviewee responses on qualitative questions
about involvement in budget oversight, direct supervision of management, and
spending discretion ceded to management by the board. The last three questions
we refer to as ``coded qualitative'' (CQ) questions. In each case, we trained
interviewers to ask an open ended questions, with further probing questions,
and then to code answers as indicated by the rubric in table \ref{tab:rubric}.
Codes range between 1 and 5, where in each case a relatively low number refers
to ``weak'' monitoring and a relatively high number refers to ``strong''
monitoring. There is no single obvious means for assessing ``monitoring
precision'' as it's characterized in our model by the parameter
$\sigma_{\mu}^2$. We interpret answers to our questions as proxies for this
quantity, and consider various functional specifications of all three proxies.
It also is important to recognize that it is reasonable to think of montoring
precision as an endogenous choice that board members make. Considering this
endogenouity formally would significantly complicate our model so for now we
settle on an exogenous specification and a cautious interpretation of causlity.
Whatever relation exists between monitoring intensity and incentives we
consider as mostly descriptive for now. In future work we hope to consider
endogeneity explicitly.

\subsection*{Estimation Results}

Table \ref{tab:pbp_results} contains estimation results for explicit pay for
peformance. Each equation is estimated as a binary choice probit model. Given
that we do not have direct access to \emph{actual} performance-based pay,
there is likely significant reporting error in respondents assessment of
typical performance-based-pay magnitudes. In cases where a portion of the
performance-based-pay is discretionary, there is also the potential for
misunderstanding about what's meant by ``formulaic'' versus ``discretionary''
performance-based pay. Where therefore code Incentive as binary, with a 1
indicating some level of performance-based-pay, based on the assumption that
we have less measurement error in this quantity. We do the same with the various
monitoring-intensity measures. This is a difficult analytic construct to
measure empirically. We focus on the degree of involvement by the board in
setting the annual budget, limits on annual off-budget spending, and
communication with management staff other than the CEO. We sum the three and code
anything above a 9 (of a maximum 15) as relatively ``intensive'' monitoring.

The table reports estimated coefficients and p-values for each variable on
each equation. We used the same set of regressors in each equation, adding
progressively more controls. Across all specifications, there is strong
evidence that CEOs with more members rely more heavily on performance-based
pay. The same is true for producer cooperatives, relative to consumer
cooperatives. There is also evidence suggesting that cooperatives with better
information tend more often to use performance-based pay. Interestingly,
characteristics of the CEO also explain to some extent the use of performance-
based pay. In particular, more educated CEOs, and CEOs with longer tenures,
tend more often to receive performance-based pay.

Table \ref{tab:implicit_results} reports results from a similar regression,
but where we look at the subsample of firms that claim to use
performance-based pay. Among these, we construct the variable Implicit, by
coding the value 1 if the firm claims to use discretionary or subjective
incentives as part of its overall pay-for-performance strategy, and 0
otherwise. Here we continue to see a positive, but much weaker, relationship
between firm size the use of discretionary incentives. The effect that seems
to be most important is whether the cooperative is composed of producers or
consumers. Consistent with our theory, consumer cooperatives tend to rely more
heavily on implicit incentives.

\subsubsection*{Comparison with Investor Owned Firms}

Comparing our data with results in our review of related literature, there
significant qualitative differences between CEO pay practices in cooperative
and noncooperative firms. Most importantly, performance-based pay is a much
smaller fraction of total pay, and there is much more use of implicit and
subjective pay by cooperative firms. Both differences are consistent with the
notion that cooperative boards must soften line incentives in order encourage
effort devoted to service flows, and that service flows are noncontractible.

As noted earlier, the relationship between firm size and ceo pay-for-
performance sensitivity can plausibly be positive or negative. On the one
hand, if CEO effort has a multiplicative effect on firm value, then an
increase in firm size increases the marginal product effort and should lead to
more highly-powered incentives. On the other hand, CEO effort may not be
multiplicative to the extent that to a large organization mutes the impact of
any one individual on outcomes. Simiarly, if CEOs of larger firms are paid a
greater base salary, the percentage of it which is needed to motivate effort
may be relatively small. Nevertheless, there is no good reason to expect
systematic differences in these relationships across cooperative and
noncooperative firms. And yet, we see exactly the opposite relationship
between firm size (measured by number of members) and pay-for-performance
sensitivity in cooperative firms relative to noncooperative firms. One
plausible explanation for this that is based on our model is that d
decision-making costs increase with membership size and therefore may lead the
firm to opt for more objective measures of executive performance.





