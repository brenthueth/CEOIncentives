% !TEX root = main.tex

The determinants of CEO pay levels and sensitivity of pay to performance have
been widely study by economists. While classical theory predicts that firm's
profitability should be the ultimate driver of CEO compensation level, most
early empirical studies show that firm \emph{size}, either measured by sales or
assets, appears to be an important explanatory variable of the cross-sectional
variability of CEO pay, whereas profitability remains insignificant
\citep{CiscelCarroll1980}. \citet{JensenMurphy1990} provide a seminal
contribution that shifts the attention toward the relationship between firm
size and its CEO pay sensitivity measured as the dollar change in CEO wealth
per dollar change in firm value. Beside their widely-cited result showing a
tenuous link between CEO pay-performance sensitivity and shareholder wealth,
Jensen and Murphy show that pay-performance sensitivity decreases with firm
size. Some authors critisize this result on the grounds that the magnitude of
contingent bonuses and the power of an incentive scheme cannot be equated. As
argued by \citet{Holmstrom1992} and \citet{Rosen1992}, although CEOs of large
firms do receive small level of contingent explicit bonuses, they usually hold
a small (but sizable) fraction of their wealth in the form of their own
company's stock. Arguably, when the firm is large, even small adverse swings
in stock price can have important consequences for CEO wealth. As such, CEOs
of large companies have strong incentives to raise share prices without
requiring high a level of contingent bonus.\footnote{Taking into account these
critics, \citet{Schaefer1998} studies the relationship between firm size and
performance-pay sensitivity. Using a standard model of bonus scheme to guide
his empirical analysis, he finds that pay sensitivity is inversely
proportional to the square root of firm size.} In a more recent contribution,
\citet{BakerHall2004} develop a model in which they assume that the marginal
product of a CEO's action on firm value \emph{can} depend on a firm's size.
When such a dependence is allowed for, they show that the level of explicit
incentives can decrease with increased firm size while keeping overall
incentives roughly constant.

\citet{jenter2010ceo} provide a recent review of this literature and summarize
empirical findings noting that between 1992 and 2008, salary fell from an
average 42\% of total pay to 17\%, with bonus and long-term incentives
remaining relatively steady at 20-30\%. Most of the change has come from the
increased use of options and stock-based pay.


% Our work contributes to this literature by examining how explicit incentives
% vary with firm size in a setting where equity ownership by the CEO is not
% feasible.\footnote{Cooperative statutes limit ownership participation to
% members (the CEO is an employee), and cooperative equity generally is not
% traded. Some cooperatives operate systematic equity redemption programs, but
% redeem at par value.}

To the best of our knowledge, there exists very few contributions that study
executive compensation in member-owned firms. The scarce empirical evidence
available suggests, though, that executive compensation in member-owned firms
has a strong subjective component. \citet{HuethMarcoul2009} report evidence
from a small number of CEO interviews. Although this study uses too few
observations to provide statistical evidence, there is a clear pattern:
cooperative CEOs face mostly subjective and discretionary pay. In each case,
annual bonuses are based on an evaluation of the CEO performance using soft
information collected by board members. In general, interviews with board
members reveal that such evaluation were performed ``'in the board room'' and
that CEOs did not precisely know what bonus amount they would receive.
Arguably, this finding contrasts somewhat with usual compensation practices
observed in for-profit firms.\footnote{For instance, discussing the nature of
executives annual bonus contracts in for profit corporations, \citet[][, p.
11]{Murphy1999} writes `` one result that emerges in the study of CEO
compensation...is that annual bonus contracts are largely explicit, with at
most a limited role for discretion.''} Nevertheless, even if subjective
performance measures seem less used in for-profit corporations, they are used
somewhat. Using a sample of bonus plans offered by 280 publicly and privately
held companies, \citet{oyermurphy2003} show that firms indeed exercise
discretion in awarding these bonuses. However, they note that discretion plays
a more prominent role in privately held firms and, in particular, when the firm
size increases.

Our theoretical model takes stock of the recurrent casual observation that
member-owned firms do often resort to implicit incentives and uses an implicit
framework that builds on the work of \citet{BakerGibbonsMurphy1994}. Like
\citet{Schottner2008}, we study relational contracting in a multitasking
context. More precisely, we assume that the CEO is instructed rewarded based on
a bottom-line performance measure, and on a noncontractible service to members.
CEO performance is then (subjectively) appraised by the board who award a
bonus. In most of the contribution of the implicit contracting literature, the
principal distorts its payment to the agent in order to satisfy an enforcement
constraint \citep{Bull1987,BakerGibbonsMurphy1994,Levin2003}. We assume instead
that, \emph{ex ante}, the board can commit to acquire soft information to
refine its appraisal of the manager's performance.
\citet{cornelli2013monitoring} show empirically that soft information acquired
by board members plays a much larger role than hard data to appraise manager's
performance. In effect, the principal is able to satisfy the enforcement
constraint by altering its information structure rather than by proposing
distorted payments to the agent.

The next section develops a multi-tasking model of implicit incentives and
monitoring, and the subsequent section discusses our data. We then test several
hypotheses that emerge from our model. The final section concludes.
