We consider an environment where the board of a cooperative firm, representing
its \textit{n} members, must provide incentives to a risk neutral manager. The
manager is instructed to pursue two objectives that we assume are
\emph{equally} important for the membership.\footnote{Different weight for
these two objectives can easily be incorporated in our theoretical framework.}
Members derive utility both from a profit flow $\pi$ that arises with
probability $p_{\pi}$ but also from a non contractible provision of user
services, $\phi\left(  n\right)  ,$ successfully arising with probability
$p_{\phi}$.\footnote{In a food consumer cooperatives, $\phi$ may represent
some non tangible attributes of the good such as whether it is
\textquotedblleft organic\textquotedblright\ and/or locally produced. In
producer cooperative, a milk producer cooperative may offer services such as
daily milk collection.} Formally, the firm maximizes the following objective
function%
\begin{equation}
p_{\pi}\cdot\pi+p_{\phi}\cdot\phi\left(  n\right)  .\label{firmobjective}%
\end{equation}
We define $\phi\left(  n\right)  $ as a function giving the maximum
\textquotedblleft user benefit\textquotedblright\ that is \emph{agreeable}\ by
the median customer in a cooperative firm with a membership of size
$n$.\footnote{In the remainder, we will drop the argument $n$ for simplicity.}
We assume that $\partial\phi\left(  n\right)  /\partial n\leq0$ meaning that,
as the number of members increases, the level of user services agreeable by a
majority of members is diminished.\footnote{In the remainder, we will often
omit the argument $n$ and replace $\phi\left(  n\right)  $ by $\phi$ for
simplicity.} This assumption is in many ways a shortcut for a complex process
during which members not only identify the range of services that can be
offered by the firm but also collectively decide which level will actually be
offered. We think of this process as dynamic in nature. Intuitively, the
cooperative firm is initially formed by founding members who are likely\emph{
the most eager} to obtain the service (say the market does not offer this good
\emph{and} service combination) and, as such, the initial level of service
required by founding member may actually be high. When the firm grows and
accepts new members, the new (median) member is less likely to insist as much
as the founding members on getting the service. So eventually, the service
level will go down as $n$ increases.\footnote{More precisely, assume that
members face an initial fixed investment of value $K>0$ that must be incurred
before the good or service can be enjoyed. Assume further that providing
multiple levels of service to members is prohibitively costly so that members
must collectivelly decide which unique level of service they want through a
vote with a majority rule. Denote by $\phi_{i}$ the marginal benefit of member
$i$ for a unit of service level provided by the cooperative and assume without
loss of generality that $\phi_{1}>\phi_{2}>...>\phi_{n}$. When there are $n$
members in the cooperative, the median voter, $m$, will no longer
\textquotedblleft accept\textquotedblright\ an additional member so long as
$\phi_{m-1}-\phi_{m}>K/\left(  n-1\right)  -K/n=K/n\left(  n-1\right)  $ and
$\phi_{m}-\phi_{m+1}<K/n-K/\left(  n+1\right)  =K/n\left(  n+1\right)  .$
\par
That is, the loss in service induced by an additional less eager member must
be compensated by the lower cost of providing this service. Note that if $n$
is increased for exogenous reasons (say $K$ becomes lower), then the
equilibrium size of the cooperative will increase but the equilibrium level of
service provided will decrease.}

Another rationale, perhaps more specific to consumer cooperative, is to think
about this firm as offering a collection of attributes

\emph{Production.}\textbf{\ }We assume that whether $\pi$ and $\phi$ are
delivered to the shareholders successfully depends on the effort provided by
the manager and we abstract from any other production input. More precisely,
the manager expands two types of effort toward the two parts of the objective
function and he can either fail or be successful on each of the tasks assigned
by the board. Formally,\emph{\ }the probability of success on profit, $p_{\pi
}$, is defined as%
\[
p_{\pi}=\Pr\left(  y_{1}=1\mid e_{1}\right)  =e_{1}%
\]
while the probability of success on the provision of services, $p_{\phi}$, is
defined as%
\[
p_{\phi}=\Pr\left(  y_{2}=1\mid e_{2}\right)  =e_{2}.
\]
When he exerts effort levels $\left(  e_{1},e_{2}\right)  ,$the manager's
labor cost is defined as%
\[
C\left(  e_{1},e_{2}\right)  =\frac{e_{1}^{2}}{2}+\frac{e_{2}^{2}}{2}+\theta
e_{1}e_{2}\text{ with }\theta\in\left[  0,1\right]  ,
\]
where $\theta\in\left[  0,1\right]  $ denotes the conflict between these two
tasks as perceived by the manager. For him, increasing effort on the services
provided raises the marginal cost of working on the \textquotedblleft bottom
line\textquotedblright, and vice versa. In our setting, it seems natural to
assume that increasing (the accounting measure of) net profit by, say,
decreasing expenses on services to member must (at some point at least)
conflict with a higher level of member satisfaction.

\emph{Judging managerial performance. }We assume that effort levels expanded
by the manager are \emph{not observable} by the board. Judging whether the
manager has satisfied customers is arguably a complex task for the board and
it is inherently prone to judgement errors and subjectivity. We assume that
when it comes to service to members, the board can only make subjective
assessment. Although, these assessments can be precise we assume that no
legally enforceable contract can be written on them. In general, we expect
that the board will base its assessment of the manager's effectiveness on
\emph{some signal of success}. Signals of success are relatively scarce and
they are common knowledge for both the manager and the board. For instance,
the board may infer that there is a good performance when there is an accrual
of new members in the firm. Yet, even if this occurrence is often associated
with strong managerial effort on customers satisfaction, it can also happen
for reasons that are remotely related to it. For instance, a weaker level of
service offered by a competitor (or a much lower price) can explain a surge in
the membership level and is unrelated to current managerial effort. Similarly,
the absence of formal complaints by members may also be associated with
satisfactory performance on the second task but again can happen for reasons
unrelated to manager's effort.

In our model, we assume that the signals on which the board bases it
assessment tracks down real success imperfectly and randomly. Formally, we let
the probability of successful signal, be
\[
p_{s}=\Pr\left(  S_{2}=1\mid e_{2}\right)  \equiv\mu e_{2}=\mu p_{\phi}%
\]
where $\mu$ is a positive random variable affecting manager's success signal
and privately observed by the manager. The uncertainty about $\mu$ is
represented by a continuous probability density function $f\left(  \mu\right)
$. Without much loss of generality, we assume that $E\left[  \mu\right]  =%
%TCIMACRO{\tint \nolimits_{\mathbb{R} _{+}}}%
%BeginExpansion
{\textstyle\int\nolimits_{\mathbb{R} _{+}}}
%EndExpansion
\mu f\left(  \mu\right)  d\mu\equiv1$ and $var\left[  \mu\right]  =%
%TCIMACRO{\tint \nolimits_{\mathbb{R} _{+}}}%
%BeginExpansion
{\textstyle\int\nolimits_{\mathbb{R} _{+}}}
%EndExpansion
\mu^{2}f\left(  \mu\right)  d\mu\equiv\sigma_{\mu}^{2}<\infty$.

In our model, the assumption $E\left[  \mu\right]  =1$ means that the board
will observe an incorrect but \emph{unbiased} signal of success. More
precisely, a low $\mu$ (i.e. $\mu<1$) represents a state of nature where the
manager knows that the success signal tend to be biased against him. As such,
when $\mu<1$, his effort will, all else equal, be less likely to trigger a
signal of success. Alternatively, when $\mu$ is high, the same level of effort
will be more likely to trigger a signal of success.

In our baseline model, it is important to note that any inefficiency of the
managerial incentives scheme will be due to the inability of the board to
perfectly assess managerial successes or failures.\footnote{In fact, when
$\sigma_{\mu}^{2}\rightarrow0$, the first best can be achieved.}

The parameter $\sigma_{\mu}^{2}$ represents the precision of the board's
observations of $\mu$ and thus its ability to judge the manager's performance.
While we treat $\sigma_{\mu}^{2}$ as an exogenous variable, in practice, a
lower level of $\sigma_{\mu}^{2}$ can be achieved through several costly
corporate rules. For instance, increasing the frequency of board meetings
should reduce $\sigma_{\mu}^{2}$. Likewise, decreasing spending limits or
decreasing the threshold for asset divestiture without board approval forces
the manager to explain his action to directors. Finally, Board members living
close to the firm's headquarters and/or consuming firm's products or services
can to a large extent be considered as a commitment to increase the precision
of the observation of $\mu$.

\emph{Timing of events. }The game proceeds as follows:

\begin{enumerate}
\item The compensation scheme is offered to the manager. The total salary,
$w,$ is made of a fixed part, $s$, and a bonus structure based on the two
tasks, $(b_{1},b_{2})$. Manager decide to accept or reject this contract.

\item $\mu$ is realized and privately observed by the manager.

\item Manager effort levels are performed and firm outcomes are realized.
Manager is rewarded. Note that for the second task the board only observe a
non contractible signal before deciding whether to award $b_{2}$ or not.
\end{enumerate}

We now the optimal incentives scheme when the manager has no limited liability.

\section{$\allowbreak$ $\allowbreak$Incentive Scheme with no Limited
Liability}

We analyze the case with no limited Liability and we assume for now that both
task outcomes are contractible.\footnote{We will later relax this assumption
by introducing an implicit contracting framework.} The board offers a salary
that is affine in the bonuses, that is $w=s+p_{\pi}b_{1}+p_{s}b_{2}$. Given
that $\mu$ is realized \emph{after} the manager has accepted the compensation
scheme, when the manager accepts the board offer, his salary is \emph{de
facto} a random variable. He accepts this offer if it dominates his second
best option, $w_{0},$in expectation. Note that after observing The generic
program of the firm can be written as%

\[
\underset{\left(  e_{1},e_{2},s,b_{1},b_{2}\right)  }{Max}\int_{\mathbb{R}%
_{+}}\text{ }\left\{  e_{1}\pi+e_{2}\phi-w\right\}  d\mu
\]
subject to%
\begin{equation}
\int_{\mathbb{R}_{+}}\left\{  s+e_{1}b_{1}+\mu e_{2}b_{2}-C\left(  e_{1}%
,e_{2}\right)  \right\}  d\mu\geq w_{0} \tag{IR}%
\end{equation}
and%

\begin{equation}
\left(  e_{1},e_{2}\right)  \in\arg\max e_{1}^{\prime}b_{1}+e_{2}^{\prime}\mu
b_{2}-\frac{e_{1}^{2^{\prime}}}{2}-\frac{e_{2}^{2^{\prime}}}{2}-\theta
e_{1}^{\prime}e_{2}^{\prime} \tag{IC}%
\end{equation}
and where%
\[
w=s+e_{1}b_{1}+\mu e_{2}b_{2}.
\]
Given a bonus structure $(b_{1},b_{2})$, the manager's first order conditions
are%
\[
e_{1}=b_{1}-\theta e_{2}%
\]
and%
\[
e_{2}=\mu b_{2}-\theta e_{1}%
\]
Solving the system above yields the optimal levels of effort%
\begin{equation}
e_{1}^{\ast}=\max\left(  0,\frac{b_{1}-\theta\mu b_{2}}{1-\theta^{2}}\right)
\text{ and }e_{2}^{\ast}=\max\left(  0,\frac{\mu b_{2}-\theta b_{1}}%
{1-\theta^{2}}\right)  . \label{optimaleffortbonus}%
\end{equation}
Note that in this analysis we focus on interior effort solution. This
restriction will imply some restriction on the parameter that will be computed later.

Given the specification of the manager's tasks, we have $\partial e_{1}^{\ast
}/\partial b_{1}>0$ and $\partial e_{1}^{\ast}/\partial b_{2}<0$, that is the
board will draw away the manager's attention to task 1 by increasing the bonus
offered on task 2.

\emph{Determination of total compensation }$w$. The manager accepts the
compensation contract if the IR constraint is fulfilled, that is if%

\begin{equation}%
\begin{array}
[c]{c}%
s+E_{\mu}\left\{
\begin{array}
[c]{c}%
\left(  \frac{b_{1}-\theta\mu b_{2}}{\left(  1-\theta^{2}\right)  }\right)
b_{1}+\left(  \frac{\mu b_{2}-\theta b_{1}}{\left(  1-\theta^{2}\right)
}\right)  \mu b_{2}-\frac{1}{2}\left(  \frac{b_{1}-\theta\mu b_{2}}%
{1-\theta^{2}}\right)  ^{2}\\
\text{ \ \ \ \ \ \ \ \ \ \ \ \ \ \ }-\frac{1}{2}\left(  \frac{\mu b_{2}-\theta
b_{1}}{1-\theta^{2}}\right)  ^{2}-\theta\frac{b_{1}-\theta\mu b_{2}}%
{1-\theta^{2}}\frac{\mu b_{2}-\theta b_{1}}{1-\theta^{2}}%
\end{array}
\right\}  \geq w_{0}.
\end{array}
\tag{IR}%
\end{equation}
After a little bit of algebra and noting that $E\left(  \mu^{2}\right)
=1+\sigma_{\mu}^{2}$, we obtain%

\begin{equation}%
\begin{array}
[c]{c}%
s+\frac{b_{1}^{2}+\left(  1+\sigma_{\mu}^{2}\right)  b_{2}^{2}-2\theta
b_{1}b_{2}}{2\left(  1-\theta^{2}\right)  }\geq w_{0}%
\end{array}
\tag{IR}%
\end{equation}
Thus, compensation scheme will be accepted if%

\begin{equation}%
\begin{array}
[c]{c}%
s\geq s^{\ast}\equiv w_{0}-\frac{b_{1}^{2}+\left(  1+\sigma_{\mu}^{2}\right)
b_{2}^{2}-2\theta b_{1}b_{2}}{2\left(  1-\theta^{2}\right)  }.
\end{array}
\tag{IR}%
\end{equation}
The expected total wage is thus%
\[%
\begin{array}
[c]{c}%
w=\text{ }\underset{s^{\ast}}{\underbrace{w_{0}-\frac{b_{1}^{2}+\left(
1+\sigma_{\mu}^{2}\right)  b_{2}^{2}-2\theta b_{1}b_{2}}{2\left(  1-\theta
^{2}\right)  }}}+E_{\mu}\left\{  \frac{b_{1}-\theta\mu b_{2}}{1-\theta^{2}%
}b_{1}+\frac{\mu b_{2}-\theta b_{1}}{1-\theta^{2}}\mu b_{2}\right\}
\end{array}
.
\]
or after basic algebra yields%
\[
w^{\ast}\left(  b_{1},b_{2}\right)  =w_{0}+\frac{\sigma_{\mu}^{2}b_{2}%
^{2}+b_{1}^{2}-2\theta b_{1}b_{2}+b_{2}^{2}}{2\left(  1-\theta^{2}\right)  }.
\]
Replacing $w^{\ast}$ in the objective function, we obtain%
\[
\underset{\left(  b_{1},b_{2}\right)  }{Max}\text{ }E_{\mu}\left\{
\frac{b_{1}-\theta\mu b_{2}}{1-\theta^{2}}\pi+\frac{\mu b_{2}-\theta b_{1}%
}{1-\theta^{2}}\phi\right\}  -\frac{\sigma_{\mu}^{2}b_{2}^{2}+b_{1}%
^{2}-2\theta b_{1}b_{2}+b_{2}^{2}}{2\left(  1-\theta^{2}\right)  }-w_{0}%
\]
or, after applying expectation operator,%
\begin{equation}
\underset{\left(  b_{1},b_{2}\right)  }{Max}\text{ }\frac{b_{1}-\theta b_{2}%
}{1-\theta^{2}}\pi+\frac{b_{2}-\theta b_{1}}{1-\theta^{2}}\phi-\frac
{\sigma_{\mu}^{2}b_{2}^{2}+b_{1}^{2}-2\theta b_{1}b_{2}+b_{2}^{2}}{2\left(
1-\theta^{2}\right)  }-w_{0}\label{objectivebonus}%
\end{equation}
First order conditions yield the following system of two equations with two
unknowns%
\[
\left\{
\begin{array}
[c]{c}%
b_{1}=\pi-\theta\phi+\theta b_{2}\\
\left(  1+\sigma_{\mu}^{2}\right)  b_{2}=\phi-\pi\theta+\theta b_{1}%
\end{array}
\right.  .
\]
Plugging in the first expression into $b_{2}$, we obtain the equilibrium
values for the bonuses as%
\begin{equation}
b_{2}=\frac{1-\theta^{2}}{1-\theta^{2}+\sigma_{\mu}^{2}}\phi\label{b2optimal}%
\end{equation}
and
\begin{equation}
b_{1}=\max\left(  0,\pi-\frac{\theta\sigma_{\mu}^{2}}{1-\theta^{2}+\sigma
_{\mu}^{2}}\phi\right)  .\label{b1optimal}%
\end{equation}
$\allowbreak$We see that these two expressions embody two particular cases.
First, when the manager perceives no conflict between the two tasks, that is
when $\theta\rightarrow0$, then the first best is reached for task 1 and
$b_{1}^{\ast}=\pi$ while the gap between task 2's performance signal and its
true value causes $b_{2}^{\ast}=\phi/\left(  1+\sigma_{\mu}^{2}\right)  $ to
be less than $\phi$ for any $\sigma_{\mu}^{2}>0$. Second, when there is no gap
between the true performance and its signal (i.e. $\sigma_{\mu}^{2}%
\rightarrow0$), then first best is achieved as $b_{1}^{\ast}=\pi$ and
$b_{2}^{\ast}=$ $\phi$ in this case.

In our setting, the existing conflict between the two tasks may lead the board
to abandon one type of task. The following proposition characterizes
conditions under which all regimes exists

\begin{proposition}
Assume $\pi>0$, $\sigma_{\mu}^{2}>0$. For any $\theta<\overline{\theta}$,
there exist two thresholds $\underline{\phi}$ and $\overline{\phi}$ such that
$\underline{\text{ }\phi}<\overline{\phi}$. In this case, we have the
following set of regimes for increasing values of $\phi.$
\end{proposition}

\begin{lemma}
\begin{enumerate}
\item For $\phi\leq\underline{\phi}\equiv\pi\theta\frac{1-\theta^{2}%
+\sigma_{\mu}^{2}}{1-\theta^{2}+\theta^{2}\sigma_{\mu}^{2}}\left(
1+\sqrt{\frac{\left(  1-\theta^{2}\right)  \sigma_{\mu}^{2}}{1-\theta
^{2}+\sigma_{\mu}^{2}}}\right)  $, the board optimally sets $b_{1}^{\ast}=\pi$
and $b_{2}^{\ast}=0$, and the manager only works on task 1.

\item For $\underline{\phi}<\phi<\overline{\phi}\equiv\frac{\pi}{\theta}%
\frac{\left(  \sigma_{\mu}^{2}+1\right)  \left(  1-\theta^{2}+\sigma_{\mu}%
^{2}\right)  }{1-\theta^{2}+\sigma_{\mu}^{2}+\sigma_{\mu}^{4}}\left(
1-\sqrt{\frac{\left(  1-\theta^{2}\right)  \sigma_{\mu}^{2}}{\left(
\sigma_{\mu}^{2}+1\right)  \left(  1-\theta^{2}+\sigma_{\mu}^{2}\right)  }%
}\right)  $, the board sets $b_{1}^{\ast}=\pi-\frac{\theta\sigma_{\mu}^{2}%
}{1-\theta^{2}+\sigma_{\mu}^{2}}\phi$ and $b_{2}^{\ast}=\frac{1-\theta^{2}%
}{1-\theta^{2}+\sigma_{\mu}^{2}}\phi,$ and the manager exerts a positive
effort on both tasks.

\item For $\phi\geq\overline{\phi}$, the board sets $b_{1}^{\ast}=0$ and
$b_{2}^{\ast}=\frac{1}{1+\sigma_{\mu}^{2}}\phi$, and the manager only works on
task 2.
\end{enumerate}
\end{lemma}

\begin{proposition}
For $\theta\geq\overline{\theta}$ (high conflict level), no incentives on both
tasks are given in equilibrium for any $\phi\geq0$.
\end{proposition}

Point 3 of lemma 1 shows that if the membership is very keen on the service
provided by the cooperative (i.e. $\phi$ is large compared to $\pi$), members
may altogether decide to set $b_{1}^{\ast}=0$, that is, abandon any managerial
incentives for the bottom line part of the firm's objective. Conversely, if
members strongly prefer profit (i.e. $\pi$ is large compared to $\phi$), then
members will decide to set $b_{2}^{\ast}=0$ and abandon incentives for
\textquotedblleft service to members.\textquotedblright\ When the conflict
between the two tasks perceived by the manager is too strong a multitasking
incentives scheme is abandonned. The next result provides comparative statics
for $\left(  b_{1}^{\ast},b_{2}^{\ast}\right)  $.

\begin{proposition}
[membership size]For the equilibrium bonuses $\left(  b_{1}^{\ast},b_{2}%
^{\ast}\right)  $, we have the following comparative statics\newline

\begin{enumerate}
\item When $\theta$ increases, both bonuses will decrease.

\item When the membership size n increases, the \textquotedblleft bottom
line\textquotedblright\ bonus, $b_{1}^{\ast}$, increases. Likewise, the
\textquotedblleft service to members\textquotedblright\ bonus, $b_{2}^{\ast}
$, decreases.

\item The bottom line bonus, $b_{1}^{\ast}$, and the service bonus,
$b_{2}^{\ast}$ Increase with the monitoring precision $1/\sigma_{\mu}^{2}$.
\end{enumerate}
\end{proposition}

\begin{proof}
Proof is straightforward and is omitted.
\end{proof}

A notable feature of this model is that when the information obtained on the
service becomes less precise, both task are affected through the managerial
cost of effort; that is both bonuses decrease with $\sigma_{\mu}^{2}.$ For
future reference, we can obtain the firm's profit equilibrium value using the
optimal bonuses and replacing them in the objective function. After
rearranging terms, the equilibrium profit of the cooperative firm is%
\[
\Pi_{12}\left(  b_{1}^{\ast},b_{2}^{\ast}\right)  =\frac{\pi^{2}+\phi
^{2}-2\theta\pi\phi}{2\left(  1-\theta^{2}\right)  }-\frac{\sigma_{\mu}%
^{2}\phi^{2}}{2\left(  1-\theta^{2}+\sigma_{\mu}^{2}\right)  }-w_{0}.
\]
$\allowbreak\allowbreak$

\bigskip As one would expect, it is increasing with $1/\sigma_{\mu}^{2}$, the
monitoring precision of the board. We now relax the assumption that task 2's
outcome is contractible.

\subsection{Using implicit contracts on task 2}

In the previous section, we have assumed that service to members was fully
contractible \emph{ex ante}. In practice though, many cooperatives do award a
good performance bonus based on an appraisal of managerial performed \emph{ex
post}; i.e. after the facts (Hueth and Marcoul, 2008).

In our setting, we assume that the manager and the board do agree on the
nature of the quality of service signal but they can never convince a third
party. In each period, a bonus structure $\left(  b_{1},b_{2}\right)  $ is
decided and after firm performance is realized, the board must decide to
reward the manager on the quality of service task. Potentially, we assume that
the board can renege on its promise to reward the manager with $b_{2}$ in case
of success. If the board renege, trust between the board and the manager is
broken and we assume that he exerts $e_{2}=0$ on the non contractible task
forever after.

When the cooperative enters an implicit contract with the manager. Using the
profit expression established earlier, the generic program of the cooperative
is written as%

\begin{equation}
\underset{\left(  b_{1},b_{2}\right)  }{Max}\text{ }\frac{b_{1}-\theta b_{2}%
}{1-\theta^{2}}\pi+\frac{b_{2}-\theta b_{1}}{1-\theta^{2}}\phi-\frac
{\sigma_{\mu}^{2}b_{2}^{2}+b_{1}^{2}-2\theta b_{1}b_{2}+b_{2}^{2}}{2\left(
1-\theta^{2}\right)  }-w_{0} \tag{$P_I$}%
\end{equation}
subject to%
\begin{equation}
\frac{\Pi\left(  b_{1}^{\ast},b_{2}^{\ast}\right)  -\Pi\left(  b_{1}%
^{s},0\right)  }{r}\geq b_{2}^{\ast} \label{renegeconstraint}%
\end{equation}
The non-renege constraint states that the loss from reneging on the (implicit)
bonus promise is higher that the short-run gain of not paying the manager's
bonus. In the constraint, $r$ is the discount rate common to both the manager
and the board. Thus, $\left(  \Pi\left(  b_{1}^{\ast},b_{2}^{\ast}\right)
-\Pi\left(  b_{1}^{s},0\right)  \right)  /r$ denotes the discounted value of
the future stream of profit coming from a trustful relationship with the manager.

As is standard in the literature on implicit contracting, we assume that the
manager use a trigger strategy. In order to solve program $\mathcal{P}_{I}$,
we must determine what is $\Pi\left(  b_{1},0\right)  $. Literally,
$\Pi\left(  b_{1},0\right)  $ is the value for the board of its relationship
with the manager after it has reneged on its promise to pay him $b_{2}^{\ast}%
$. In a trigger strategy, the manager no longer trust the board after it has
reneged once and decides to exert effort 0 on the non contractible task.

Without effort on task 2, the objective of the board is%
\[
\underset{\left(  e_{1},s,b_{1},\right)  }{Max}e_{1}\pi-w
\]
subject to%
\begin{equation}
s+e_{1}b_{1}-C\left(  e_{1},0\right)  \geq w_{0} \tag{IR}%
\end{equation}
and%

\begin{equation}
\left(  e_{1},e_{2}\right)  \in\arg\max e_{1}^{\prime}b_{1}-\frac
{e_{1}^{2^{\prime}}}{2} \tag{IC}%
\end{equation}
and where%
\[
w=s+e_{1}b_{1}.
\]


The optimal solution is such that and
\[
e_{1}=b_{1}\text{, }s+\frac{b_{1}^{2}}{2}\geq w_{0}\text{ and }w=s+b_{1}^{2}.
\]
so%
\[
s\geq s^{\ast}=w_{0}-\frac{b_{1}^{2}}{2}\text{ and }w^{\ast}=s^{\ast}%
+b_{1}^{2}=w_{0}+\frac{b_{1}^{2}}{2}%
\]
Thus, the cooperative solves
\[
\underset{b_{1}}{Max}b_{1}\pi-\frac{b_{1}^{2}}{2}-w_{0}%
\]
yielding%
\[
b_{1}^{s}=\pi\text{ and profit }\Pi\left(  b_{1}^{s},0\right)  =\frac{\pi^{2}%
}{2}-w_{0}%
\]
Using this value, the program of the firm is rewritten as%
\begin{equation}%
\begin{array}
[c]{c}%
\underset{\left(  b_{1},b_{2}\right)  }{Max}\text{ }\frac{b_{1}-\theta b_{2}%
}{1-\theta^{2}}\pi+\frac{b_{2}-\theta b_{1}}{1-\theta^{2}}\phi-\frac
{\sigma_{\mu}^{2}b_{2}^{2}+b_{1}^{2}-2\theta b_{1}b_{2}+b_{2}^{2}}{2\left(
1-\theta^{2}\right)  }-w_{0}%
\end{array}
\label{Mainprogram}%
\end{equation}
subject to the renege constraint%
\begin{equation}%
\begin{array}
[c]{c}%
\frac{b_{1}-\theta b_{2}}{1-\theta^{2}}\pi+\frac{b_{2}-\theta b_{1}}%
{1-\theta^{2}}\phi-\frac{\sigma_{\mu}^{2}b_{2}^{2}+b_{1}^{2}-2\theta
b_{1}b_{2}+b_{2}^{2}}{2\left(  1-\theta^{2}\right)  }-\frac{\pi^{2}}{2}\geq
rb_{2}%
\end{array}
\label{renegeconstraint1}%
\end{equation}


When this constraint is binding both $b_{1}$ and $b_{2}$ are related. If the
cooperative board wants to keep implicit contracting relationship with the
manager, it has to limit the level of bonus given for service to members. More
precisely $b_{1}$ and $b_{2}$ should verify (\ref{renegeconstraint1}) with
equality. If we denote be $\overline{b}_{2}\left(  b_{1}\right)  $ the
solution of (\ref{renegeconstraint1}), we have%
\begin{equation}%
\begin{array}
[c]{c}%
\overline{b}_{2}\left(  b_{1}\right)  =\frac{\phi-r-\pi\theta+\theta
b_{1}+r\theta^{2}+\sqrt{\left[  \phi-r-\pi\theta+\theta b_{1}+r\theta
^{2}\right]  ^{2}-\left[  b_{1}^{2}-2\left(  p-\theta\phi\right)
b_{1}+\left(  1-\theta^{2}\right)  \pi^{2}\right]  \left(  \sigma_{\mu}%
^{2}+1\right)  }}{1+\sigma_{\mu}^{2}}%
\end{array}
.\label{constrainedB2}%
\end{equation}
Then the optimal constrained bonus $\overline{b}_{1}^{\ast}$ is solution of
\[%
\begin{array}
[c]{c}%
\underset{b_{1}}{Max}\text{ }\frac{b_{1}-\theta\left(  \overline{b}_{2}\left(
b_{1}\right)  \right)  }{1-\theta^{2}}\pi+\frac{\left(  \overline{b}%
_{2}\left(  b_{1}\right)  \right)  -\theta b_{1}}{1-\theta^{2}}\phi
-\frac{\sigma_{\mu}^{2}\left(  \overline{b}_{2}\left(  b_{1}\right)  \right)
^{2}+b_{1}^{2}-2\theta b_{1}\left(  \overline{b}_{2}\left(  b_{1}\right)
\right)  +\left(  \overline{b}_{2}\left(  b_{1}\right)  \right)  ^{2}%
}{2\left(  1-\theta^{2}\right)  }%
\end{array}
.
\]
After eliminating the negative root, we obtain%
\[%
\begin{array}
[c]{c}%
\overline{b}_{1}^{\ast}=\frac{\pi\left(  1-\theta^{2}+\sigma_{\mu}^{2}\right)
-\theta\sigma_{\mu}^{2}\phi-r\theta+r\theta^{3}+\theta\sqrt{\allowbreak\left(
\left(  1-\theta^{2}\right)  \left(  \phi-r\right)  \right)  ^{2}%
+\theta\left(  \pi^{2}\theta+\theta\phi^{2}-2\pi\phi\right)  \left(
1-\theta^{2}+\sigma_{\mu}^{2}\right)  }}{1-\theta^{2}+\sigma_{\mu}^{2}}%
\end{array}
,
\]
$\allowbreak$and after replacing in (\ref{constrainedB2}), we get
\[%
\begin{array}
[c]{c}%
\overline{b}_{2}^{\ast}=\frac{\left(  1-\theta^{2}\right)  \left(
\phi-r\right)  +\sqrt{\left(  \left(  1-\theta^{2}\right)  \left(
\phi-r\right)  \right)  ^{2}+\theta\left(  \pi^{2}\theta+\theta\phi^{2}%
-2\pi\phi\right)  \left(  1-\theta^{2}+\sigma_{\mu}^{2}\right)  }}%
{1-\theta^{2}+\sigma_{\mu}^{2}}%
\end{array}
\]
We denote the optimal solution by $\left(  \overline{b}_{1},\overline{b}%
_{2}\right)  $. The next proposition characterizes the solution the program
$\mathcal{P}_{I}$.

\begin{proposition}
Assume that $\theta<\overline{\theta}.$Then there exists a strictly positive
threshold $\overline{r}$\bigskip\ such that for any $r\in\left]  0,\min\left(
\overline{r},\frac{\pi}{\theta},\frac{1}{2}\left(  \overline{\phi}-\pi
^{2}\left(  1+\sigma_{\mu}^{2}\right)  /\overline{\phi}\right)  \right)
\right]  $ and we have two strictly positive values of $\phi$, $\overline
{\phi}_{1}\left(  r\right)  $ and $\overline{\phi}_{2}\left(  r\right)  $,
verifying $\underline{\phi}<\overline{\phi}_{1}\left(  r\right)
<\overline{\phi}_{2}\left(  r\right)  $ and such that 

\begin{enumerate}
\item If $\phi\leq\underline{\phi}$, (or equivalently if the membership size
$n$ is very large), the first best is implemented without implicit contracting
on task 1

\item If $\underline{\phi}\leq\phi\leq\overline{\phi}_{1}\left(  r\right)  $
(or equivalently if the membership size $n$ is rather large), implicit
contracts cannot be sustained. No incentives for members' services are
provided, that is $b_{2}^{s}=0$ and $b_{1}^{s}=\pi$.

\item If $\overline{\phi}_{1}\left(  r\right)  \leq\phi\leq\overline{\phi}%
_{2}\left(  r\right)  $ (or equivalently if the membership size $n$ is
intermediate), then constraint (\ref{renegeconstraint1}) is binding the
manager's incentive scheme\ is given by $\left(  \overline{b}_{1}^{\ast
},\overline{b}_{2}^{\ast}\right)  .$

\item If $\overline{\phi}_{2}\left(  r\right)  \leq$ $\phi\leq\overline{\phi}$
(or equivalently the membership size $n$ is small) then the manager
compensation scheme is given by (\ref{b1optimal}) and (\ref{b2optimal}).

\item If $\phi>\overline{\phi}$, there is implicit contracting on task 2 only.
\end{enumerate}
\end{proposition}

\begin{proof}
\bigskip The proof is the appendix.
\end{proof}

The parameter configuration of this proposition have been chosen so that we
obtain the maximum number of bonus schemes. If some of the conditions on $r$,
then less different bonus schemes will be obtained in equilibrium. In general,
the logic of this proposition is identical to that of proposition 1. In
particular,when the number of members is large, the incentive scheme proposed
to the manager is solely based on profit performance.

In the next section, we mostly investigate the empirical implication of
proposition 1 and we leave the empirical implications of proposition 2 for
future research.
